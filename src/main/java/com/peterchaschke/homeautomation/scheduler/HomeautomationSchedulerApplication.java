package com.peterchaschke.homeautomation.scheduler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HomeautomationSchedulerApplication {

	public static void main(String[] args) {
		SpringApplication.run(HomeautomationSchedulerApplication.class, args);
	}

}
