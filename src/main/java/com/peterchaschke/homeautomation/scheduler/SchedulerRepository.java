package com.peterchaschke.homeautomation.scheduler;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface SchedulerRepository extends JpaRepository<Schedule, Long> {

	@Query(value = "SELECT * FROM scheduler where "
			+ "type = 'HOURLY' AND MINUTE(time) = :minute AND active = 1 OR "
			+ "type = 'DAILY' AND HOUR(time) = :hour AND MINUTE(time) = :minute AND active = 1 OR "
			+ "type = 'WEEKLY' AND JSON_CONTAINS(days, :dayOfWeekStr) AND HOUR(time) = :hour AND MINUTE(time) = :minute AND active = 1 OR "
			+ "type = 'MONTHLY' AND day = :dayOfMonth AND HOUR(time) = :hour AND MINUTE(time) = :minute AND active = 1 OR "
			+ "type = 'YEARLY' AND day = :dayOfYear AND HOUR(time) = :hour AND MINUTE(time) = :minute AND active = 1 OR "
			+ "type = 'FIXED' AND date = :date AND HOUR(time) = :hour AND MINUTE(time) = :minute AND active = 1", nativeQuery = true)
	public List<Schedule> getSchedules(@Param("minute") int minute, @Param("hour") int hour, @Param("dayOfWeekStr") String dayOfWeekStr,
			@Param("dayOfMonth") int dayOfMonth, @Param("dayOfYear") int dayOfYear, @Param("date") String date);


}
