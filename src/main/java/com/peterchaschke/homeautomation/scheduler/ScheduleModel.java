package com.peterchaschke.homeautomation.scheduler;

import java.sql.Date;
import java.sql.Time;

import org.json.JSONArray;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@RequiredArgsConstructor
public class ScheduleModel {
	
	private Long id;
	private SchedulerType type;
	private Integer minute;
	private Integer day;
	private JSONArray days;
	private Date date;
	private Time time;
	private int active;
	private String name;
	private Long actionId;

}
