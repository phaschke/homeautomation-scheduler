package com.peterchaschke.homeautomation.scheduler.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/scheduler")
public class SchedulerController {
	
	@GetMapping("/status")
	public ResponseEntity<?> status() {
		
		return new ResponseEntity<>("Success", HttpStatus.OK);
	}
}
