package com.peterchaschke.homeautomation.scheduler.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

@Configuration
public class SecurityConfiguration {
	
	@Value("${cors.allowed.urls}")
	private List<String> corsAllowedURLs;

	@Bean
	CorsFilter corsFilter() {

		CorsConfiguration configuration = new CorsConfiguration();
		
		configuration.setAllowedOrigins(corsAllowedURLs);
		configuration.setAllowedMethods(Arrays.asList("GET"));
		configuration.setAllowedHeaders(Arrays.asList("Content-Type", "Access-Control-Allow-Origin",
				"Access-Control-Expose-Headers", "Access-Control-Allow-Headers", "Authorization", "X-Requested-With",
				"requestId", "Correlation-Id"));
		
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return new CorsFilter(source);
	}

}
