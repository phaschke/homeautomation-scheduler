package com.peterchaschke.homeautomation.scheduler;

import java.sql.Date;
import java.sql.Time;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.json.JSONArray;
import org.json.JSONException;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "scheduler")
@Getter
@Setter
@RequiredArgsConstructor
public class Schedule {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;
	
	@Column(name = "name")
	private String name;
	
	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private SchedulerType type;
	
	@Column(name = "minute")
	private Integer minute;
	
	@Column(name = "day")
	private Integer day;
	
	@Column(name = "days")
	private String days;
	
	@Column(name = "date")
	private Date date;
	
	@Column(name = "time")
	private Time time;
	
	@Column(name = "active")
	private int active;
	
	@Column(name = "action_id")
	private Long actionId;
	
	public ScheduleModel toModel() {
		
		ScheduleModel scheduleModel = new ScheduleModel();
		
		scheduleModel.setId(this.getId());
		scheduleModel.setName(this.getName());
		scheduleModel.setType(this.getType());
		scheduleModel.setMinute(this.getMinute());
		scheduleModel.setDay(this.getDay());
		try {
			scheduleModel.setDays(new JSONArray(this.getDay()));
		} catch (JSONException e) {
			scheduleModel.setDays(null);
		}
		scheduleModel.setDate(this.getDate());
		scheduleModel.setTime(this.getTime());
		scheduleModel.setActive(this.getActive());
		scheduleModel.setActionId(this.getActionId());
		
		return scheduleModel;
		
	}
}
