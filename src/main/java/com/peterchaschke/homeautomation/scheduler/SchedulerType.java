package com.peterchaschke.homeautomation.scheduler;

public enum SchedulerType {
	HOURLY,
	DAILY,
	WEEKLY,
	MONTHLY,
	YEARLY,
	FIXED
}
