package com.peterchaschke.homeautomation.scheduler;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableScheduling
@EnableAsync
@Service
public class SchedulerService {
	
	private static final Logger logger = LoggerFactory.getLogger(SchedulerService.class);

	private static final int TIMEOUT = 3000;
	
	@Value("${core.action.url}")
	private String coreActionUrl;
	
	@Autowired
	private final SchedulerRepository schedulerRepository;
	
	public SchedulerService(SchedulerRepository schedulerRepository) {
		this.schedulerRepository = schedulerRepository;
	}
	
	@Async
	@Scheduled(cron = "0 * * * * *")
	protected void getSchedules() {
		
		ZoneId zoneId = ZoneId.of("America/Denver");
		LocalDateTime dateTime = LocalDateTime.now(zoneId);
		
		int minute = dateTime.getMinute();
		int hour = dateTime.getHour();
		int dayOfWeek = dateTime.getDayOfWeek().getValue();
		int dayOfMonth = dateTime.getDayOfMonth();
		int dayOfYear = dateTime.getDayOfYear();
		String date = dateTime.format(DateTimeFormatter.ISO_LOCAL_DATE).toString();
		
		//System.out.println("minute: "+minute +" hour: " + hour +" dayOfWeek: " + dayOfWeek +" dayOfMonth: "+ dayOfMonth + " dayOfYear: " + dayOfYear + " date: "+date);
		
		List<Schedule> schedules = schedulerRepository.getSchedules(minute, hour, Integer.toString(dayOfWeek), dayOfMonth, dayOfYear, date);
		
		runActions(schedules);
		
	}
	
	private void runActions(List<Schedule> schedules) {
		
		String scheduleNames = "scheduleNames: ";
		
		for(int i = 0; i < schedules.size(); i++) {
			scheduleNames += schedules.get(i).getName();
			makeRequest(schedules.get(i).getActionId());
		}
		
		logger.info("Matching schedules: " + scheduleNames);

	}
	
	private void makeRequest(Long actionId) {
		
		try {
			RestTemplate restTemplate = new RestTemplate();
			HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();
			requestFactory.setConnectTimeout(TIMEOUT);
			requestFactory.setReadTimeout(TIMEOUT);

			restTemplate.setRequestFactory(requestFactory);
			
			String url = coreActionUrl + actionId;
			
			logger.info("Request url: " + url);
			
			HttpEntity<?> requestEntity = null;
			restTemplate.exchange(url, HttpMethod.PATCH, requestEntity, Void.class);
			
			logger.info("Successfully executed action with id: " + actionId);
			
		} catch(Exception e) {
			
			logger.error("Failed to request action: " + e.getMessage());
		}
		
	
	}

}
